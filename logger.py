import json
import os
import sys
import collections

class Logger:
  def __init__(self, name, checkpoint_metric='val_loss', metric_aggregator=min, logdir='logs', save_checkpoints=True):
    self.directory = os.path.join(logdir, name)
    os.makedirs(self.directory, exist_ok=True)
    self.metrics = collections.defaultdict(dict)
    self.checkpoint_metric = checkpoint_metric
    self.metric_aggregator = metric_aggregator
    self.hparams = {}
    self.best_loss = None
    self.best_checkpoint = os.path.join(self.directory, 'best_checkpoint')
    self.test_metrics = {}
    self.save_function = None
    self.save_checkpoints = save_checkpoints

  def set_save_function(self, save_function):
    self.save_function = save_function

  def log_metrics(self, epoch, metrics):
    self.metrics[epoch].update(metrics)
    if self.save_checkpoints:
      self.save_function(os.path.join(self.directory, 'last_checkpoint'))
    if self.checkpoint_metric in metrics and (self.best_loss is None or self.metric_aggregator(metrics[self.checkpoint_metric], self.best_loss) == metrics[self.checkpoint_metric]):
      self.best_loss = metrics[self.checkpoint_metric]
      if self.save_checkpoints:
        self.save_function(os.path.join(self.directory, 'best_checkpoint'))
    self.save()

  def log_test(self, metrics):
    self.test_metrics = metrics
    self.save()

  def log_hparams(self, hparams):
    self.hparams = vars(hparams)
    self.save()

  def save(self):
    with open(os.path.join(self.directory, 'run.json'), 'w') as fp:
      fp.write(json.dumps({
        'metrics': self.metrics, 
        'hparams': self.hparams, 
        'test': self.test_metrics, 
        'best_loss': self.best_loss,
      }, indent=2))


if __name__ == '__main__':
  import bottle
  import glob
  logdir = sys.argv[1] if len(sys.argv) > 1 else 'logs'

  @bottle.route('/<metric>')
  def metric(metric='val_loss'):
    series = []
    for path in glob.glob(logdir + '/*/*.json'):
      with open(path) as fp:
        logs = json.loads(fp.read())
        values = [x[metric] for epoch, x in sorted(logs['metrics'].items(), key=lambda k: int(k[0])) if metric in x]
        if len(values) > 0: 
          series.append({
            'values': values,
            'name': '\n'.join(['%s = %s' % (k, str(v)) for k, v in logs['hparams'].items()])
          })
    bottle.response.content_type = 'application/json'
    return json.dumps(series)

  @bottle.route('/')
  def index():
    metrics = set()
    for path in glob.glob('logs/*/*.json'):
      with open(path) as fp:
        logs = json.loads(fp.read())
        for row in logs['metrics'].values():
          metrics.update(row.keys())

    buttons = '<div id="buttons">' + ' | '.join(['<a href="#" onclick="update(\'%s\')">%s</a>' % (metric, metric) for metric in sorted(metrics)]) + ' | <input name="filter" type="text" id="filter" placeholder="filter"></div>'
    html = buttons + """<canvas id="canvas">
    <script src="https://pageperso.lis-lab.fr/benoit.favre/files/autoplot.js"></script>
    <script>
      var selected_metric;
      function update(metric) {
        selected_metric = metric;
        fetch('/' + metric).then(res => res.json()).then(series => {
          chart('canvas', series);
        });
      }
      setInterval(function() {
        update(selected_metric);
      }, 60 * 1000);
      update('%s');
    </script>""" % sorted(metrics)[0]
    
    return html

  bottle.run(host='localhost', port=6006, quiet=True)

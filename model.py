from argparse import ArgumentParser
import sys

import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from pytorch_lightning.core.lightning import LightningModule

from transformers import AutoTokenizer
from transformers import AutoModel

import data

# based on https://www.kaggle.com/rejpalcz/best-loss-function-for-f1-score-metric
def binary_f1_score_with_logits(y_pred, y_true):
  epsilon = 1e-7
  y_pred = torch.sigmoid(y_pred)
  y_true = y_true.float()
  
  tp = (y_true * y_pred).sum(dim=0).float()
  tn = ((1 - y_true) * (1 - y_pred)).sum(dim=0).float()
  fp = ((1 - y_true) * y_pred).sum(dim=0).float()
  fn = (y_true * (1 - y_pred)).sum(dim=0).float()

  precision = tp / (tp + fp + epsilon)
  recall = tp / (tp + fn + epsilon)

  f1 = 2 * (precision * recall) / (precision + recall + epsilon)
  f1 = f1.clamp(min=epsilon, max=1 - epsilon)
  #f1 = torch.where(torch.isnan(f1), torch.zeros_like(f1), f1)

  return 1 - f1.mean()

class RNNLayer(nn.Module):
  def __init__(self, hidden_size=128, dropout=0.3):
    super().__init__()
    rnn_layers = 1
    directions = 2
    rnn_output = hidden_size * rnn_layers * directions
    self.rnn = nn.GRU(hidden_size, hidden_size, bias=True, num_layers=1, bidirectional=(directions == 2), batch_first=True)
    self.dense = nn.Linear(rnn_output, hidden_size)
    self.dropout = nn.Dropout(dropout)
    self.norm = nn.LayerNorm(hidden_size)

  def forward(self, x):
    output, hidden = self.rnn(x)
    layer = self.dropout(F.gelu(self.dense(output))) + x
    return self.norm(layer)


class RNN(nn.Module):
  def __init__(self, vocab_size, embed_size, hidden_size, num_layers, dropout, padding_idx=0):
    super().__init__()
    self.embed = nn.Embedding(vocab_size, embed_size, padding_idx=padding_idx)
    self.embed_to_rnn = nn.Linear(embed_size, hidden_size)
    self.layers = nn.ModuleList([RNNLayer(hidden_size=hidden_size, dropout=dropout) for i in range(num_layers)])
    self.dropout = nn.Dropout(dropout)

  def forward(self, x):
    embed = self.dropout(self.embed(x))
    activations = self.embed_to_rnn(F.gelu(embed))
    for layer in self.layers:
      activations = layer(activations)
    pool = F.max_pool1d(activations.transpose(1, 2), activations.size(1))
    return pool.view(x.shape[0], -1)


class CNNLayer(nn.Module):
  def __init__(self, hidden_size, kernel_size, dropout):
    super().__init__()
    self.conv = nn.Conv1d(hidden_size, hidden_size, kernel_size=kernel_size)
    self.dropout = nn.Dropout(dropout)
    self.norm = nn.LayerNorm(hidden_size)

  def forward(self, x):
    output = self.conv(x.transpose(1, 2)).transpose(2, 1)
    missing = x.shape[1] - output.shape[1]
    output = torch.cat([output, torch.zeros(x.shape[0], missing, x.shape[2], device=x.device)], 1)
    layer = self.dropout(F.gelu(output)) + x
    return self.norm(layer)

class CNN(nn.Module):
  def __init__(self, vocab_size, embed_size, hidden_size, num_layers, kernel_size, dropout, padding_idx=0):
    super().__init__()
    self.embed = nn.Embedding(vocab_size, embed_size, padding_idx=padding_idx)
    self.embed_to_cnn = nn.Linear(embed_size, hidden_size)
    self.layers = nn.ModuleList([CNNLayer(hidden_size=hidden_size, kernel_size=kernel_size, dropout=dropout) for i in range(num_layers)])
    self.dropout = nn.Dropout(dropout)

  def forward(self, x):
    embed = self.dropout(self.embed(x))
    activations = self.embed_to_cnn(F.gelu(embed))
    for layer in self.layers:
      activations = layer(activations)
    pool = F.max_pool1d(activations.transpose(1, 2), activations.size(1))
    return pool.view(x.shape[0], -1)


class Model(LightningModule):

  def __init__(self, hparams):
    super().__init__()

    self.hparams = hparams
    self.epoch = 1
    self.tokenizer = AutoTokenizer.from_pretrained(hparams.bert_flavor)
    self.train_set, self.valid_set, self.test_set, self.label_vocab = data.load(self.tokenizer, hparams)
    hparams.num_labels = len(self.label_vocab)

    if hparams.model == 'bert':
      self.bert = AutoModel.from_pretrained(hparams.bert_flavor)
      if self.hparams.transfer:
        print('loading bert weights from checkpoint "%s"' % self.hparams.transfer)
        checkpoint = torch.load(self.hparams.transfer)
        state_dict = {x[5:]: y for x, y in checkpoint['state_dict'].items() if x.startswith('bert.')}
        self.bert.load_state_dict(state_dict)
      decision_input_size = self.bert.config.hidden_size

    elif hparams.model == 'rnn':
      self.rnn = RNN(self.tokenizer.vocab_size, hparams.rnn_embed_size, hparams.rnn_hidden_size, hparams.rnn_layers, hparams.dropout, self.tokenizer.pad_token_id)
      decision_input_size = self.hparams.rnn_hidden_size
    
    elif hparams.model == 'cnn':
      self.cnn = CNN(self.tokenizer.vocab_size, hparams.cnn_embed_size, hparams.cnn_hidden_size, hparams.cnn_layers, hparams.cnn_kernel_size, hparams.dropout, self.tokenizer.pad_token_id)
      decision_input_size = self.hparams.cnn_hidden_size

    else:
      raise ValueError('invalid model type "%s"' % hparams.model)

    self.decision = nn.Linear(decision_input_size, hparams.num_labels)
    self.dropout = nn.Dropout(hparams.dropout)
    if self.hparams.loss == 'bce':
      self.loss_function = F.binary_cross_entropy_with_logits
    elif self.hparams.loss == 'f1':
      self.loss_function = binary_f1_score_with_logits
    else:
      raise ValueError('invalid loss "%s"' % self.hparams.loss)

  def forward(self, x):
    if self.hparams.model == 'bert':
      _, output = self.bert(x, attention_mask = (x != self.tokenizer.pad_token_id).long())
    elif self.hparams.model == 'rnn':
      output = self.rnn(x)
    elif self.hparams.model == 'cnn':
      output = self.cnn(x)
    else:
      raise ValueError('invalid model type "%s"' % self.hparams.model)

    decision = self.decision(F.gelu(self.dropout(output)))
    return decision

  def training_step(self, batch, batch_idx):
    x, y = batch
    y_hat = self(x)
    loss = self.loss_function(y_hat, y)
    return {'loss': loss}

  def validation_step(self, batch, batch_idx):
    x, y = batch
    y_hat = self(x)
    loss = self.loss_function(y_hat, y)
    bce = F.binary_cross_entropy_with_logits(y_hat, y)
    acc_correct = torch.sum((y_hat >= 0) == y)
    acc_num = torch.tensor([y.shape[0] * y.shape[1]])
    num_correct = torch.sum((y_hat >= 0) * (y == 1))
    num_hyp = torch.sum(y_hat >= 0)
    num_ref = torch.sum(y == 1)
    return {'val_loss': loss, 'bce': bce, 'num_correct': num_correct, 'num_ref': num_ref, 'num_hyp': num_hyp, 'acc_correct': acc_correct, 'acc_num': acc_num}

  def test_step(self, batch, batch_idx):
    x, y = batch
    y_hat = self(x)
    loss = self.loss_function(y_hat, y)
    bce = F.binary_cross_entropy_with_logits(y_hat, y)
    acc_correct = torch.sum((y_hat >= 0) == y)
    acc_num = torch.tensor([y.shape[0] * y.shape[1]])
    num_correct = torch.sum((y_hat >= 0) * (y == 1))
    num_hyp = torch.sum(y_hat >= 0)
    num_ref = torch.sum(y == 1)
    return {'test_loss': loss, 'bce': bce, 'num_correct': num_correct, 'num_ref': num_ref, 'num_hyp': num_hyp, 'acc_correct': acc_correct, 'acc_num': acc_num}

  def training_epoch_end(self, outputs):
    avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
    self.epoch += 1
    return {'loss': avg_loss, 'log': {'loss': avg_loss}}

  def validation_epoch_end(self, outputs):
    metrics = outputs[0].keys()
    values = {metric: torch.stack([x[metric] for x in outputs]) for metric in metrics}

    avg_loss = values['val_loss'].mean()

    bce = values['bce'].mean()
    num_correct = values['num_correct'].sum()
    acc_num = values['acc_num'].sum()
    accuracy = values['acc_correct'].sum() / float(acc_num.item())
    num_ref = values['num_ref'].sum()
    num_hyp = values['num_hyp'].sum()
    recall = num_correct / float(num_ref.item()) if num_ref != 0 else torch.tensor([0])
    precision = num_correct / float(num_hyp.item()) if num_hyp != 0 else torch.tensor([0])
    fscore = 2 * recall * precision / float((precision + recall).item()) if precision + recall != 0 else torch.tensor([0])

    log_metrics = {'bce': bce.item(), 'accuracy': accuracy.item(), 'recall': recall.item(), 'precision': precision.item(), 'fscore': fscore.item()}
    self.custom_logger.log_metrics(self.epoch, log_metrics)

    return {'val_loss': avg_loss}

  def test_epoch_end(self, outputs):
    metrics = outputs[0].keys()
    values = {metric: torch.stack([x[metric] for x in outputs]) for metric in metrics}

    avg_loss = values['test_loss'].mean()

    bce = values['bce'].mean()
    num_correct = values['num_correct'].sum()
    acc_num = values['acc_num'].sum()
    accuracy = values['acc_correct'].sum() / float(acc_num.item())
    num_ref = values['num_ref'].sum()
    num_hyp = values['num_hyp'].sum()
    recall = num_correct / float(num_ref.item()) if num_ref != 0 else torch.tensor([0])
    precision = num_correct / float(num_hyp.item()) if num_hyp != 0 else torch.tensor([0])
    fscore = 2 * recall * precision / float((precision + recall).item()) if precision + recall != 0 else torch.tensor([0])

    log_metrics = {'bce': bce.item(), 'accuracy': accuracy.item(), 'recall': recall.item(), 'precision': precision.item(), 'fscore': fscore.item()}
    self.custom_logger.log_test(log_metrics)

    return {'test_loss': avg_loss}

  def configure_optimizers(self):
    optimizer = torch.optim.AdamW(self.parameters(), lr=self.hparams.learning_rate)
    scheduler = None
    if self.hparams.scheduler == 'warmup_linear':
      num_warmup_steps = self.hparams.scheduler_warmup
      num_training_steps = self.hparams.epochs

      def lr_lambda(current_step):
          if current_step < num_warmup_steps:
              return float(current_step) / float(max(1, num_warmup_steps))
          return max( 0.0, float(num_training_steps - current_step) / float(max(1, num_training_steps - num_warmup_steps)) )
      scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda, -1)
    elif self.hparams.scheduler != None:
      raise ValueError('invalid scheduler "%s"' % self.hparams.scheduler)

    if scheduler:
      return [optimizer], [scheduler]
    else:
      return optimizer

  def collate_fn(self, inputs):
    text_len = max([len(x[0]) for x in inputs])
    x_text = torch.full((len(inputs), text_len), self.tokenizer.pad_token_id).long()
    for i, x in enumerate(inputs):
      x_text[i, :len(x[0])] = torch.LongTensor(x[0])
    y = torch.tensor([x[-1] for x in inputs]).float()
    return x_text, y

  def train_dataloader(self):
    return DataLoader(self.train_set, batch_size=self.hparams.batch_size, shuffle=True, pin_memory=True, collate_fn=self.collate_fn)

  def val_dataloader(self):
    return DataLoader(self.valid_set, batch_size=self.hparams.batch_size, pin_memory=True, collate_fn=self.collate_fn)

  def test_dataloader(self):
    return DataLoader(self.test_set, batch_size=self.hparams.batch_size, pin_memory=True, collate_fn=self.collate_fn)

  @staticmethod
  def add_model_specific_args(parent_parser):
    parser = ArgumentParser(parents=[parent_parser])
    parser.add_argument('--stem', type=str, required=True, help='stem name of json files containing training/validation/test instances (<stem>.{train,valid,test})')
    parser.add_argument('--learning_rate', default=2e-5, type=float, help='learning rate (default=2e-5)')
    parser.add_argument('--batch_size', default=32, type=int, help='size of batch (default=32)')
    parser.add_argument('--epochs', default=20, type=int, help='number of epochs (default=20)')
    parser.add_argument('--valid_size_percent', default=10, type=int, help='validation set size in %% (default=10)')
    parser.add_argument('--max_len', default=256, type=int, help='max sequence length (default=256)')
    parser.add_argument('--selected_features', default=['title', 'abstract'], nargs='+', type=str, help='list of features to load from input (default=title abstract)')
    parser.add_argument('--dropout', default=.3, type=float, help='dropout after bert')
    parser.add_argument('--loss', default='bce', type=str, help='choose loss function [f1, bce] (default=bce)')
    parser.add_argument('--augment_data', default=False, action='store_true', help='simulate missing abstract through augmentation (default=do not augment data)')
    parser.add_argument('--transfer', default=None, type=str, help='transfer weights from checkpoint (default=do not transfer)')
    parser.add_argument('--model', default='bert', type=str, help='model type [rnn, cnn, bert] (default=bert)')
    parser.add_argument('--bert_flavor', default='monologg/biobert_v1.1_pubmed', type=str, help='pretrained bert model (default=monologg/biobert_v1.1_pubmed)')
    parser.add_argument('--rnn_embed_size', default=128, type=int, help='rnn embedding size (default=128)')
    parser.add_argument('--rnn_hidden_size', default=128, type=int, help='rnn hidden size (default=128)')
    parser.add_argument('--rnn_layers', default=1, type=int, help='rnn number of layers (default=1)')
    parser.add_argument('--cnn_embed_size', default=128, type=int, help='cnn embedding size (default=128)')
    parser.add_argument('--cnn_hidden_size', default=128, type=int, help='cnn hidden size (default=128)')
    parser.add_argument('--cnn_layers', default=1, type=int, help='cnn number of layers (default=1)')
    parser.add_argument('--cnn_kernel_size', default=3, type=int, help='cnn kernel size (default=3)')
    parser.add_argument('--scheduler', default=None, type=str, help='learning rate schedule [warmup_linear] (default=fixed learning rate)')
    parser.add_argument('--scheduler_warmup', default=1, type=int, help='learning rate schedule warmup epochs (default=1)')

    return parser


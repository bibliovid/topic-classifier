import collections
import json
import random

import torch
from torch.utils.data import Dataset

class CustomDataset(Dataset):
  def __init__(self, texts, labels):
    self.texts = texts   
    self.labels = labels

  def __getitem__(self, index):
    return self.texts[index], self.labels[index]
  
  def __len__(self):
    return len(self.labels)

def bert_text_to_ids(tokenizer, sentence, max_len):
  return torch.tensor(tokenizer.encode(sentence, add_special_tokens=True, max_length=max_len))

def to_int(tokenizer, label_vocab, hparams, dataset):
  int_texts = []
  int_labels = []
  sorted_labels = list(sorted(label_vocab.keys(), key=label_vocab.get))

  for i, article in enumerate(dataset):
    for feature in hparams.selected_features:
      if feature not in article or article[feature] is None:
        article[feature] = ''
    text = ' | '.join([''.join(article[feature]) for feature in hparams.selected_features])
    int_texts.append(bert_text_to_ids(tokenizer, text, hparams.max_len))
    int_labels.append([1 if 'topics' in article and label in article['topics'] else 0 for label in sorted_labels])
    if hparams.augment_data and i > hparams.valid_size: # don't forget to skip valid set
      text = ' | '.join([''.join(article[feature] if feature != 'abstract' else '') for feature in hparams.selected_features])
      int_texts.append(bert_text_to_ids(tokenizer, text, hparams.max_len))
      int_labels.append([1 if 'topics' in article and label in article['topics'] else 0 for label in sorted_labels])

  return int_texts, int_labels

def load(tokenizer, hparams):

  with open(hparams.stem + '.train') as fp:
    train_articles = json.loads(fp.read())

  with open(hparams.stem + '.valid') as fp:
    valid_articles = json.loads(fp.read())

  with open(hparams.stem + '.test') as fp:
    test_articles = json.loads(fp.read())

  label_vocab = collections.defaultdict(lambda: len(label_vocab))

  for article in train_articles:
    if 'topics' in article:
      for topic in article['topics']:
        label_vocab[topic]

  label_vocab = dict(label_vocab)

  train_dataset = [article for article in train_articles if 'topics' in article] # and 'abstract' in article]
  valid_dataset = [article for article in valid_articles if 'topics' in article] # and 'abstract' in article]
  test_dataset = [article for article in test_articles if 'topics' in article] # and 'abstract' in article]
  assert len(train_dataset) > 0 and len(valid_dataset) > 0 and len(test_dataset) > 0

  #hparams.valid_size = int(hparams.valid_size_percent * len(dataset) / 100.0)
  #assert hparams.valid_size > 0

  for name, dataset in [('train', train_dataset), ('valid', valid_dataset), ('test', test_dataset)]:
    missing_abstracts = 0
    for article in dataset:
      if 'abstract' not in article or article['abstract'] == []:
        article['abstract'] = ['']
        missing_abstracts += 1
    print('WARNING: %.2f%% missing abstract in %s' % (100 * missing_abstracts / len(dataset), name))

  #random.shuffle(dataset)

  train_int_texts, train_int_labels = to_int(tokenizer, label_vocab, hparams, train_dataset)
  valid_int_texts, valid_int_labels = to_int(tokenizer, label_vocab, hparams, valid_dataset)
  test_int_texts, test_int_labels = to_int(tokenizer, label_vocab, hparams, test_dataset)

  train_set = CustomDataset(train_int_texts, train_int_labels)
  valid_set = CustomDataset(valid_int_texts, valid_int_labels)
  test_set = CustomDataset(test_int_texts, test_int_labels)
  print('training set', len(train_set))
  print('valid set', len(valid_set))
  print('test set', len(test_set))

  return train_set, valid_set, test_set, label_vocab


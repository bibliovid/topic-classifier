
data_dir = '../scrappers/data/20200615'
exp = 0
for fold in range(5):
  for dataset in ['bibliovid', 'litcovid']:
    for loss in ['bce', 'f1']:
      for learning_rate in [1e-3, 2e-5]:
        for model in ['bert', 'rnn', 'cnn']:
          if model in ['rnn', 'cnn']:
            for layers in [1, 2, 4]:
              for hidden in [128, 256, 512]:
                if model == 'cnn':
                  for kernel in [2, 3, 5]:
                    print(f"{exp} python trainer.py --gpus -1 --name exp_{exp} --epochs 20 --scheduler warmup_linear --scheduler_warmup 2 --stem '{data_dir}/folds/{dataset}-{fold}' --loss {loss} --model {model} --learning_rate {learning_rate} --{model}_layers {layers} --{model}_embed_size 256 --{model}_hidden_size {hidden} --cnn_kernel_size {kernel}")
                    exp += 1
                elif model == 'rnn':
                  print(f"{exp} python trainer.py --gpus -1 --name exp_{exp} --epochs 20 --scheduler warmup_linear --scheduler_warmup 2 --stem '{data_dir}/folds/{dataset}-{fold}' --loss {loss} --model {model} --learning_rate {learning_rate} --{model}_layers {layers} --{model}_embed_size 256 --{model}_hidden_size {hidden}")
                  exp += 1
          elif model == 'bert':
            print(f"{exp} python trainer.py --gpus -1 --name exp_{exp} --epochs 20 --scheduler warmup_linear --scheduler_warmup 2 --stem '{data_dir}/folds/{dataset}-{fold}' --loss {loss} --model {model} --learning_rate {learning_rate}")
            exp += 1

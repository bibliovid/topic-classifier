from glob import glob
import sys
import os
import json
import re
import math
import collections

def match(hparams):
  for pattern in sys.argv[1:]:
    found = False
    for name, value in hparams.items():
      key = '%s=%s' % (name, str(value))
      if pattern == key:
        found = True
        break
    if not found:
      return False
  return True

names = collections.defaultdict(list)
test_metrics = collections.defaultdict(lambda: collections.defaultdict(list))
val_metrics = collections.defaultdict(lambda: collections.defaultdict(list))
seen_hparams = collections.defaultdict(set)

for filename in glob('logs/*/run.json'):
  dirname = os.path.dirname(filename)
  if os.path.exists(dirname + '/finished'):
    with open(filename) as fp:
      experiment = json.loads(fp.read())
      hparams = experiment['hparams']
      name = hparams['name']
      del hparams['name']
      del hparams['cmd']
      del hparams['num_labels']
      hparams['stem'] = re.sub('-\d*$', '', hparams['stem'])
      if match(hparams):
        signature = json.dumps(hparams, sort_keys=True)
        names[signature].append(name)
        for metric, value in experiment['test'].items():
          test_metrics[signature][metric].append(value)
        test_metrics[signature]['val_loss'].append(experiment['best_loss'])
        for metric, value in experiment['metrics']['20'].items():
          val_metrics[signature][metric].append(value)
        for k, v in hparams.items():
          seen_hparams[k].add(json.dumps(v, sort_keys=True))

def compute_stats(values):
  values.sort()
  mean = sum(values) / len(values)
  variance = sum((x - mean) ** 2 for x in values) / len(values)
  return {'median': values[len(values) // 2], 'min': values[0], 'max': values[-1], 'mean': sum(values) / len(values), 'var': variance}

#WARN
test_metrics = val_metrics

best = None
best_hparams = None
best_signature = None
for signature in test_metrics:
  stats = {metric: compute_stats(test_metrics[signature][metric]) for metric in test_metrics[signature]}
  if best is None or stats['fscore']['mean'] > best['fscore']['mean']:
    best = stats
    best_hparams = {k: v for k, v in json.loads(signature).items()} # if len(seen_hparams[k]) > 1}
    best_signature = signature

print('fscore mean=%.4f var=%.4f' % (best['fscore']['mean'], best['fscore']['var']))
for name, value in best_hparams.items():
  print(name, '=', value)
print(names[best_signature])


#!/bin/bash

properties="(gpu IS NOT NULL) AND ((host != 'diflives1') AND (host != 'lifnode1') AND (host != 'sensei1') AND (host != 'asfalda1') AND (host != 'lisnode3'))"
find logs/ -mindepth 1 -maxdepth 1 -type d | while read dir; do
  if [ ! -f $dir/finished ]; then
    echo $dir
    rm -f "$dir/stderr" "$dir/stdout"
    oarsub -p "$properties" -l "walltime=16:00:00" -E "$dir/stderr" -O "$dir/stdout" "$dir/cmd" 
  fi
done

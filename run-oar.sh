#!/bin/bash

#properties="(gpu IS NOT NULL) AND ((host != 'diflives1') AND (host != 'lifnode1') AND (host != 'sensei1') AND (host != 'asfalda1'))"
properties="(gpu IS NOT NULL) AND ((host != 'diflives1') AND (host != 'lifnode1') AND (host != 'sensei1') AND (host != 'asfalda1') AND (host != 'lisnode3'))"
#DEBUG=--fast_dev_run
DEBUG=

while read id job; do
      dir="logs/exp_$id"
      if [ -d "$dir" ]; then
        echo ERROR: $dir already exists
        exit 1
      fi
      mkdir -p "$dir"

      cat > "$dir/cmd" << EOF
#!/bin/bash
source env/bin/activate
set -e -u -o pipefail
date > "$dir/started"
hostname >&2
echo CUDA_VISIBLE_DEVICES=\$CUDA_VISIBLE_DEVICES >&2
$job $DEBUG
date > "$dir/finished"
ls "$dir/"*checkpoint 
rm -f "$dir/"*checkpoint
EOF
      chmod +x "$dir/cmd"
      oarsub -p "$properties" -l "walltime=16:00:00" -E "$dir/stderr" -O "$dir/stdout" "$dir/cmd" 
done

